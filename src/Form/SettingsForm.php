<?php

namespace Drupal\save_continue\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings Form for module.
 *
 * @package Drupal\save_edit\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The node type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeTypeStorage;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_type_storage
   *   Node Type Storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityStorageInterface $node_type_storage) {
    parent::__construct($config_factory);
    $this->nodeTypeStorage = $node_type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity_type.manager');
    return new static(
      $container->get('config.factory'),
      $entity_manager->getStorage('node_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'save_continue.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'save_continue_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('save_continue.settings');
    $weights_range = range(-10, 10);
    $weights = array_combine($weights_range, $weights_range);

    $form['button_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to use for Save & Continue button'),
      '#description' => $this->t('This is the default text that will be used for the button at the bottom of the node form.<br>It would be best to use familiar terms like "<strong>Save &amp; Edit</strong>" or "<strong>Apply</strong>" so that users can easily understand the feature/function related to this option.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('button_value'),
    ];
    $form['button_weight'] = [
      '#type' => 'select',
      '#title' => $this->t('Save & Continue Button Weight'),
      '#description' => $this->t('You can adjust the horizontal positioning in the button section (or vertical positioning when using the dropbutton setting).'),
      '#options' => $weights,
      '#default_value' => $config->get('button_weight'),
    ];
    $form['dropbutton'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Integrate into dropbutton'),
      '#description' => $this->t('This setting will insert the Save &amp; Continue button into the save dropbutton.'),
      '#default_value' => $config->get('dropbutton'),
    ];

    $node_types = $this->nodeTypeStorage->loadMultiple();
    $keyed_node_types = [];
    foreach ($node_types as $content_type) {
      $keyed_node_types[$content_type->id()] = $content_type->label();
    }
    $default_value_node_types = $config->get('node_types');
    $form['node_types'] = [
      '#type' => 'checkboxes',
      '#options' => $keyed_node_types,
      '#title' => $this->t('Node types'),
      '#description' => $this->t('Set the node types you want to display links for.'),
      '#default_value' => $default_value_node_types ?: [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('save_continue.settings')
      ->set('button_value', $form_state->getValue('button_value'))
      ->set('button_weight', $form_state->getValue('button_weight'))
      ->set('dropbutton', $form_state->getValue('dropbutton'))
      ->set('node_types', $form_state->getValue('node_types'))
      ->save();
  }

}
