# Save & Continue

- Save & Continue is a module that provides a simple function to node editing pages.

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

- Save & Edit is a module that provides simple function to node editing pages.
- The module simply adds a button titled "Save & Continue" on node types
  selected in the admin section.
- Using this extra button when saving a node will simply redirect
  to next node of same content type in Ascending order.


## Requirements

- This module requires no modules outside of Drupal core.

## Installation

- Install the Favicon module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

## Configuration

- Admin Settings Form:
- Go to admin/config/save_continue/settings
- User can change the button text of "Save & Continue".
- User can select on which content type "Save & Continue"
  button will appear.
- Add Permission to access Admin settings form of this module.

## Maintainers

- Mahesh Patil - [maheshkp92](https://www.drupal.org/u/maheshkp92)
- Rajan Kumar  - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
